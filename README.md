# Configuration for gateway board

This repository contains configuration files for BeagleBone board, which is used as gateway for Planting House software project.

The configuration includes:

- `udev` rule to set suitable permission for serial (UART) port.
- Scripts to setup GPIO, PWM pins.
- `systemd` services to activate the scripts above.

The files are placed in folder tree that should be matched the installation in real board.

